﻿using System.Collections.Generic;
using System.Linq;

namespace SeriousGame.BasicDecypher
{
    public class BasicDecypher : IDecypher
    {
        public BasicDecypher()
        {
            PossibleSentences = new List<DecypheredSentence>();
        }

        private IList<DecypheredSentence> PossibleSentences { get; set; }

        public string[] Decypher(string cypheredSentence, DecypherData decypherData)
        {
            var result = Decypher(new DecypheredSentence(cypheredSentence), decypherData, new List<DecypheredSentence>());

            //Return only those sentences that are truely decyphered
            return result.Where(x => x.IsDecyphered).Select(x => x.DecypheredResult).ToArray();
        }

        private IList<DecypheredSentence> Decypher(DecypheredSentence sentence, DecypherData decypherData, List<DecypheredSentence> sentences)
        {
            int range = 1;
            int start = 0;

            while (range <= sentence.CypheredSentenceLeft.Length)
            {
                //Get the fragment to decypher incrementally (brute force)
                string fragmentToDecypher = sentence.CypheredSentenceLeft.Substring(start, range);

                //If there's no matching word, increase the fragment and try again
                if (!decypherData.CypheredWordPossibleWords.ContainsKey(fragmentToDecypher))
                {
                    range++;
                    continue;
                }

                //If there are on or more morse words that match, 
                var words = decypherData.CypheredWordPossibleWords[fragmentToDecypher];

                foreach (string word in words)
                {
                    //Get the fragment of the sentence that it's still cyphered
                    string leftDecypheredSentence = sentence.GetLeftFragment(fragmentToDecypher);

                    DecypheredSentence decypheredSentence = new DecypheredSentence(leftDecypheredSentence, sentence.DecypheredResult);

                    decypheredSentence.AddDecypheredWord(word);

                    //Add them to the list of results and if not fully decyphered, continue recursevily decyphering
                    sentences.Add(decypheredSentence);

                    if (!decypheredSentence.IsDecyphered)
                    {
                        Decypher(decypheredSentence, decypherData, sentences);
                    }
                }

                range++;
            }

            return sentences;
        }
    }
}
