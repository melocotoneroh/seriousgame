﻿using System;

namespace SeriousGame
{
    class Program
    {
        private static string SentenceToSolve { get { return ".--..-..-.-.-----.-----....--...-.-.-..-....--.-......----."; } }

        static void Main(string[] args)
        {
            string[] results = new BasicDecypher.BasicDecypher().Decypher(SentenceToSolve, new DecypherData());

            Console.WriteLine("The solutions for the cyphered sentence are:");

            foreach (string result in results)
            {
                Console.WriteLine(result);
            }

            Console.WriteLine("Push any key to exit...");

            Console.ReadKey();
        }
    }
}
