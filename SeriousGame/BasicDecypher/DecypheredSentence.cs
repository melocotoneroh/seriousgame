﻿using System.Text.RegularExpressions;

namespace SeriousGame.BasicDecypher
{
    public class DecypheredSentence
    {
        public DecypheredSentence(string cypheredSentence) : this (cypheredSentence, string.Empty)
        {
        }

        public DecypheredSentence(string cypheredSentence, string decypheredSentence)
        {
            CypheredSentenceLeft = cypheredSentence;
            DecypheredResult = decypheredSentence;
        }

        public string CypheredSentenceLeft { get; private set; }

        public string DecypheredResult { get; private set; }

        public bool IsDecyphered { get { return CypheredSentenceLeft.Length == 0; } }

        /// <summary>
        /// Removes an already decyphered fragment to only leave the part of the sentence that we still don't know its meaning
        /// </summary>
        /// <param name="cypheredFragment"></param>
        /// <returns></returns>
        public DecypheredSentence RemoveCypheredFragment(string cypheredFragment)
        {
            CypheredSentenceLeft = CypheredSentenceLeft.Replace(cypheredFragment, string.Empty);

            return this;
        }

        /// <summary>
        /// Adds a word to the decyphered result
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public DecypheredSentence AddDecypheredWord(string word)
        {
            DecypheredResult = DecypheredResult + word;

            return this;
        }
        /// <summary>
        /// Get the fragment of the sentence that it's still cyphered
        /// </summary>
        /// <param name="decypheredFragment"></param>
        /// <returns></returns>
        public string GetLeftFragment(string decypheredFragment)
        {
            var regex = new Regex(decypheredFragment);

            return regex.Replace(CypheredSentenceLeft, string.Empty, 1);
        }

        public override string ToString()
        {
            return DecypheredResult;
        }
    }
}
