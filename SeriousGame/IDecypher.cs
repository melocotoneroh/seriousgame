﻿namespace SeriousGame
{
    public interface IDecypher
    {
        string[] Decypher(string cypheredSentence, DecypherData decypherData);
    }
}
