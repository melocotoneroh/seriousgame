﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeriousGame
{
    public class DecypherData
    {
        /// <summary>
        /// Translation from chars to morse
        /// </summary>
        public Dictionary<string, string> CharToMorse
        {
            get
            {
                return new Dictionary<string, string>
                {
                    {"A", ".-"},
                    {"B", "-..."},
                    {"C", "-.-."},
                    {"D", "-.."},
                    {"E", "."},
                    {"F", "..-."},
                    {"G", "--."},
                    {"H", "...."},
                    {"I", ".."},
                    {"J", ".---"},
                    {"K", "-.-"},
                    {"L", ".-.."},
                    {"M", "--"},
                    {"N", "-."},
                    {"O", "---"},
                    {"P", ".--."},
                    {"Q", "--.-"},
                    {"R", ".-."},
                    {"S", "..."},
                    {"T", "-"},
                    {"U", "..-"},
                    {"V", "...-"},
                    {"W", ".--"},
                    {"X", "-..-"},
                    {"Y", "-.--"},
                    {"Z", "--.."}
                };
            }
        }

        private IList<string> possibleWords;

        /// <summary>
        /// Returns the list of most used words (the ones that are in the google1000.txt)
        /// </summary>
        public IList<string> PossibleWords
        {
            get
            {
                if (possibleWords != null)
                {
                    return possibleWords;
                }

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "SeriousGame.google1000.txt";

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    string result = reader.ReadToEnd();

                    string[] splitted = result.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                    return new List<string>(splitted);
                }
            }
        }

        private Dictionary<string, IList<string>> wordCypheredWord;

        /// <summary>
        /// Returns the pair of {morseTranslation, list of words that are translated into that morse code (can be one or more)
        /// </summary>
        public Dictionary<string, IList<string>> CypheredWordPossibleWords
        {
            get
            {
                if (wordCypheredWord != null)
                {
                    return wordCypheredWord;
                }

                wordCypheredWord = new Dictionary<string, IList<string>>();

                foreach (string word in PossibleWords)
                {
                    string cypheredWord = CypherWord(word);

                    if (wordCypheredWord.ContainsKey(cypheredWord))
                    {
                        wordCypheredWord[cypheredWord].Add(word);
                    }
                    else
                    {
                        wordCypheredWord.Add(cypheredWord, new List<string> {word});
                    }
                }

                return wordCypheredWord;
            }
        }

        private string CypherWord(string word)
        {
            string cypheredWord = string.Empty;

            foreach (char character in word)
            {
                cypheredWord += CharToMorse[character.ToString().ToUpper()];
            }

            return cypheredWord;
        }
    }
}
